# Automatización del test de Machover

## Alumnos:
-  Baltodano Beltrán, Massiel Estela.
-  Siapo Rodríguez, José Luis O.


Teoria de la aplicación del test [aqui](https://gitlab.com/JSiapo/tesis/raw/develop/Tesis/Informe/Tesis.pdf).

Teoria de tecnologia usada aqui.

## Requerimientos

### Linux
``` python
python3 -m venv venv
source venv/bin/activate
python -m pip install --upgrade pip #Update pip
pip install -r requirements.txt
```

### Windows

``` python
pip install virtualenv --user
python -m venv venv_win32
venv_win32\Scripts\activate.bat
python -m pip install --upgrade pip #Update pip
pip install -r requirements.txt
```

**Nota: para desactivar el entorno virtual**

> Linux: deactivate
> 
> Windows: venv_win32\Scripts\deactivate.bat

## Descargar data

``` python
python library/download.py arg1 arg2
```
**arg1**: link train images

**arg2**: link validation images

## Ejecutar Análisis

``` bash
docker run -it --name=tf -v ${PWD}:/tf -p 8888:8888 -p 6006:6006 tensorflow/tensorflow:nightly-py3-jupyter
```
**Sólo la primera vez**

``` bash
docker start tf
```
**Para ejecutar normalmente el contenedor**

Para abrir notebooks de carpeta **analytics**

## Ejecutar LablelImg

``` bash
labelImg
```